package com.example.eyliss.mtgxtest.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;

import com.example.eyliss.mtgxtest.MTGxApplication;
import com.example.eyliss.mtgxtest.R;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class GeneralUtils {

    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";

    /** Blur an image fast and efficiently, improving Gaussian blur
     *
     * @param image the image we want to blur
     * @param radius degree of image blur
     * @return the blurred bitmap
     *
     */
    public static Bitmap fastBlur(Bitmap image, int radius) {

        Bitmap bitmap = image.copy(image.getConfig(), true);

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = ( 0xff000000 & pix[yi] ) | ( dv[rsum] << 16 ) | ( dv[gsum] << 8 ) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }

    /** Format a byte array into a hexadecimal string
     *
     * @param bytes the byte array to format
     * @return a hexadecimal string
     *
     */
    private static String toHexString(byte[] bytes) {
        Formatter formatter = new Formatter();

        for (byte b : bytes) {
            formatter.format("%02x", b);
        }

        return formatter.toString();
    }

    /** Calculate the HMAC token using an url and the private secret key
     *
     * @param data the url created from the path, querystring, apikey and timestamp.
     * @param key the private secret key
     * @return an HMAC token
     *
     */
    public static String calculateHMAC(String data, String key)
            throws SignatureException, NoSuchAlgorithmException, InvalidKeyException
    {
        SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);
        Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
        mac.init(signingKey);
        return toHexString(mac.doFinal(data.getBytes()));
    }

    /** Append the HMAC token to the url with the required parameters. The HMAC is implemented to verify the integrity of the path,
     * querystring and apikey
     *
     * @param path the url calling the server
     * @param params parameters required for the call
     * @return  final url with the required params and hmac token included
     *
     */
    public static String appendHMACtoUrl(String path, List<NameValuePair> params){
        String query = "";

        try {
            long timestamp = System.currentTimeMillis() / 1000;
            params.add(new BasicNameValuePair("apikey", Constants.API_KEY));
            params.add(new BasicNameValuePair("timestamp",String.valueOf(timestamp)));

            String url = path + "?" + URLEncodedUtils.format(params, "ASCII");
            params.add(new BasicNameValuePair("hmac", calculateHMAC(url, Constants.API_SECRET)));

            query = path + "?" + URLEncodedUtils.format(params, "ASCII");

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        }

        return query;
    }

    /** Format a date from the server to a more readable format by the user.
     * We need to convert the date to a Calendar object, get the different elements from it and then create a new string with the new format
     *
     * @param date the string date to
     * @return the formatted string date
     *
     */
    public static String formatDate(String date){

        String formattedDate = "";
        if (!TextUtils.isEmpty(date)){
            Calendar startDate = parseStringToDate(date);
            int day = startDate.get(Calendar.DAY_OF_MONTH);
            SimpleDateFormat format = new SimpleDateFormat("MMMM");
            String month = format.format(startDate.getTime());
            int year = startDate.get(Calendar.YEAR);
            int hour = startDate.get(Calendar.HOUR_OF_DAY);
            int min = startDate.get(Calendar.MINUTE);
            int sec = startDate.get(Calendar.SECOND);
            formattedDate = MTGxApplication.getAppContext().getString(R.string.choose_event_start_date, day, month, year, hour, getFormatted(min), getFormatted(sec));
        }

        return formattedDate;
    }

    /** Convert a date in string format to a Calendar object
     *
     * @param date the string to convert
     * @return the converted Calendar object
     *
     */
    public static Calendar parseStringToDate(String date){

        String dateFormat = "yyyy-MM-dd hh:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal;
    }

    /** Format an integer depends of the number of digits. If it has one digit, a zero is concatenated for a correct date format
     *
     * @param data integer to format
     * @return the string formatted
     *
     */
    public static String getFormatted(int data){
        return data < 10 ? "0" + data : String.valueOf(data);
    }
}
