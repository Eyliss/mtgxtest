package com.example.eyliss.mtgxtest.network;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.example.eyliss.mtgxtest.MTGxApplication;

/**
 * Custom implementation of {@link com.android.volley.toolbox.StringRequest}
 */
public class MTGxStringRequest extends StringRequest {

    private static final String PROTOCOL_CONTENT_TYPE = "application/json";
    private static final String PROTOCOL_ACCEPT = "application/json";

    private Map<String, String> mParams;

    private Map<String, String> mHeaders;

    public MTGxStringRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    public MTGxStringRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(url, listener, errorListener);
    }

    public MTGxStringRequest(int method, String url, Map<String, String> headers, Map<String, String> payload, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        mHeaders = headers;
        mParams = payload;
    }


    @Override
    protected Map<String, String> getParams() {
        return this.mParams;
    }

    /**
     * Get the common headers for all our requests. We only accept application/json as content
     * type and Accept.
     *
     * @return a map that contains the common headers for all the requests.
     */
    @Override
    public Map<String, String> getHeaders() {

        Map<String, String> basicHeaders = new HashMap<String, String>();
        basicHeaders.put("Content-Type", PROTOCOL_CONTENT_TYPE);
        basicHeaders.put("Accept", PROTOCOL_ACCEPT);
        basicHeaders.put("User-agent", MTGxApplication.getUserAgent());
        basicHeaders.put("Accept-Language", Locale.getDefault().toString());

        if(mHeaders != null){
            basicHeaders.putAll(mHeaders);
        }

        return basicHeaders;
    }

    @Override
    public String getBodyContentType() {
        return "application/json";
    }

}
