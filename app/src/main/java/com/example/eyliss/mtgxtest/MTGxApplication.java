package com.example.eyliss.mtgxtest;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

public class MTGxApplication extends Application {

    private static final String TAG = "MTGxApplication";
    private static Context context;

    /**
     * Global request queue for Volley
     */
    private RequestQueue mRequestQueue;
    DisplayImageOptions displayOptions;

    /**
     * A singleton instance of the application class for easy access in other places.
     */
    private static MTGxApplication sInstance;

    public void onCreate(){
        super.onCreate();

        MTGxApplication.context = getApplicationContext();

        // Initialize the singleton
        sInstance = this;

        // Create global configuration and initialize ImageLoader with this configuration
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).build();
        ImageLoader.getInstance().init(config);

        // Create display global options for images, like image scale type and force cache.
        displayOptions = new DisplayImageOptions.Builder()
                .bitmapConfig(Bitmap.Config.RGB_565)
                .delayBeforeLoading(0)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
    }

    public static Context getAppContext() {
        return MTGxApplication.context;
    }

    /**
     *
     * @return ImageLoader display image options.
     */
    public DisplayImageOptions getDisplayOptions(){
        return displayOptions;
    }

    /**
     *
     * @return MTGxApplication singleton instance.
     */
    public static synchronized MTGxApplication getInstance() {
        return sInstance;
    }

    /**
     * Get the Volley request queue, it will be created if it is null.
     *
     * @return the Volley request queue.
     */
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    /**
     * Adds the specified request to the global queue, if the tag is specified
     * then it is used else Default TAG is used.
     *
     * @param request the request.
     * @param tag an optional tag for this request.
     */
    public <T> void addToRequestQueue(Request<T> request, String tag) {
        request.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        request.setRetryPolicy( new DefaultRetryPolicy(12000, 3, 1f));
        getRequestQueue().add(request);
    }

    public <T> void addToRequestQueue(Request<T> request) {
        // set the default tag if tag is empty
        request.setRetryPolicy( new DefaultRetryPolicy(12000, 3, 1f));
        addToRequestQueue(request, TAG);
    }

    /**
     * Cancels all pending requests by the specified TAG, it is important
     * to specify a TAG so that the pending/ongoing requests can be cancelled.
     *
     * @param tag the specified tag.
     */
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    /**
     * Get the application User Agent for Volley requests
     *
     * @return the Volley request queue.
     */
    public static String getUserAgent() {
        String packageInfo = null;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {

            Log.e("MTGx Application", "packageInfo is null");
        }

        return context.getString(R.string.app_name).replace(" ","") + "/" + packageInfo + " (android; CPU OS " + android.os.Build.VERSION.RELEASE + ")";
    }
}
