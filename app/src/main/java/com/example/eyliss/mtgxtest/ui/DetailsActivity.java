package com.example.eyliss.mtgxtest.ui;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.eyliss.mtgxtest.MTGxApplication;
import com.example.eyliss.mtgxtest.R;
import com.example.eyliss.mtgxtest.adapters.PlayedSongAdapter;
import com.example.eyliss.mtgxtest.io.Channel;
import com.example.eyliss.mtgxtest.io.Song;
import com.example.eyliss.mtgxtest.network.MTGxStringRequest;
import com.example.eyliss.mtgxtest.util.Constants;
import com.example.eyliss.mtgxtest.util.GeneralUtils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DetailsActivity extends RootActivity {

    private LinearLayout mBackgroundLayout;
    private ImageView mIvCoverArtist;
    private TextView mTvSongTitle;
    private TextView mTvSongArtist;
    private ListView mPlayedList;

    private Song mSong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_activity);

        //Song that we want to fetch the information
        mSong = getIntent().getParcelableExtra(Constants.INTENT_SONG);

        bindViews();
        setInfo();
        fetchPlayedSong();
    }

    /**
     * Bind the home layout view to use them globally in the fragment
     */
    private void bindViews(){
        mBackgroundLayout = (LinearLayout) findViewById(R.id.details_bg_layout);
        mIvCoverArtist = (ImageView) findViewById(R.id.iv_details_covert_art);
        mTvSongTitle = (TextView) findViewById(R.id.tv_details_song_name);
        mTvSongArtist = (TextView) findViewById(R.id.tv_details_artist_name);
        mPlayedList = (ListView) findViewById(R.id.played_list);
    }

    /**
     * Set song info (covert art, title and artist) into view elements. For details background a fastBlur to covert art bitmap is used.
     */
    private void setInfo(){
        if(mSong.hasCovertArt())
            ImageLoader.getInstance().loadImage(mSong.getBigCovertArt(), MTGxApplication.getInstance().getDisplayOptions(),new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    if(loadedImage != null) {
                        setBackgroundByVersion(mBackgroundLayout, new BitmapDrawable(getResources(), GeneralUtils.fastBlur(loadedImage, 20)));
                        mIvCoverArtist.setImageBitmap(loadedImage);
                    }
                }
            });

        mTvSongTitle.setText(mSong.getTitle());
        mTvSongArtist.setText(mSong.getArtistName());
    }

    /**
     * Set song info (covert art, title and artist) into view elements. For details background a fastBlur to covert art bitmap is used.
     *
     * @param view where the background has to be set
     * @param drawable to set into the view
     */
    private void setBackgroundByVersion(View view, BitmapDrawable drawable){

        if (Build.VERSION.SDK_INT >= 16) {
            view.setBackground(drawable);
        } else {
            view.setBackgroundDrawable(drawable);
        }
    }

    /**
     * Fetch the last 10 times that the song was played sorted by publish date from the newest to the older, including channel info.
     */
    private void fetchPlayedSong(){
        showDialogFragment(getString(R.string.loading_data));

        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair(Constants.PARAM_LIMIT,"10"));
        params.add(new BasicNameValuePair(Constants.PARAM_ORDER,"published_at"));
        params.add(new BasicNameValuePair(Constants.PARAM_SORT_ORDER,"desc"));
        params.add(new BasicNameValuePair(Constants.PARAM_WITH,"channel"));

        String url = Constants.SERVER_API_URL + GeneralUtils.appendHMACtoUrl(Constants.ENDPOINT_SONG +"/" + mSong.getId() + Constants.PLAYED_URL,params);
        MTGxStringRequest request = new MTGxStringRequest(Request.Method.GET,
                url,
                successPlayedSongListener,
                errorListener);

        MTGxApplication.getInstance().addToRequestQueue(request);
    }

    /**
     * Handle the played songs success requests from server.
     *
     * @return a {@link com.android.volley.Response.Listener Listener} reference.
     */
    Response.Listener successPlayedSongListener = new Response.Listener() {

        @Override
        public void onResponse(Object response) {
            hideDialogFragment();
            try {
                parsePlayedList(new JSONArray(response.toString()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Handle the played songs error requests from server.
     *
     * @return a {@link com.android.volley.Response.ErrorListener ErrorListener} reference.
     */
    Response.ErrorListener errorListener = new Response.ErrorListener() {

        @Override
        public void onErrorResponse(VolleyError volleyError) {
            hideDialogFragment();
            Toast.makeText(DetailsActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
        }
    };

    /**
     * Parse the played list response and creates two arrays: the first array "dates" includes when the element was published,
     * and the second array "channels" includes the channel where it was played each time.
     *
     * @param response received from the server with the last 10 times the song was played in JSON format
     */
    private void parsePlayedList(JSONArray response){
        ArrayList<String> dates = new ArrayList<String>();
        ArrayList<Channel> channels = new ArrayList<Channel>();
        for(int i=0; i < response.length();i++){
            try {
                JSONObject item = response.getJSONObject(i);
                dates.add(item.getString(Constants.JSON_PUBLISHED_AT));
                channels.add(new Channel(item.getJSONObject(Constants.JSON_CHANNEL)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        showPlayedList(dates, channels);
    }

    /**
     * Show the played list into the view list. It creates a new custom adapter for PlayedSong and set it to set list view.
     */
    private void showPlayedList(ArrayList<String> dates,ArrayList<Channel> channels){

        PlayedSongAdapter arrayAdapter = new PlayedSongAdapter(this, dates,channels);
        mPlayedList.setAdapter(arrayAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
