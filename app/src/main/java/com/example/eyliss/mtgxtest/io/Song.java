package com.example.eyliss.mtgxtest.io;


import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.eyliss.mtgxtest.MTGxApplication;
import com.example.eyliss.mtgxtest.network.MTGxStringRequest;
import com.example.eyliss.mtgxtest.util.Constants;
import com.example.eyliss.mtgxtest.util.GeneralUtils;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * The main song class. Represents the object and implements the necessary methods to create a song and access its data
 * It implements Parcelable to be able to pass a Song object between Android activities
 */
public class Song implements Parcelable{

    public final String TAG = "Song";

    private String mId;
    private String mArtistId;
    private String mAlbumId;
    private String mArtistName;
    private String mTitle;
    private String mSearchTitle;
    private String mLength;
    private String mCoverArt;
    private String mCovertArtSource;

    public Song (){

    }

    public Song(JSONObject album){
        super();
        updateFromJson(album);
    }

    /**
     * Update a Song object from a Json Object
     *
     * @param data JSONObject with the new object data
     */
    public void updateFromJson(JSONObject data) {
        try {

            if (!data.isNull(Constants.JSON_ID)) {
                this.mId = data.getString(Constants.JSON_ID);
            }

            if (!data.isNull(Constants.JSON_ARTIST_ID)) {
                this.mArtistId = data.getString(Constants.JSON_ARTIST_ID);
            }

            if (!data.isNull(Constants.JSON_ALBUM_ID)) {
                this.mAlbumId = data.getString(Constants.JSON_ALBUM_ID);
            }

            if (!data.isNull(Constants.JSON_ARTIST_NAME)) {
                this.mArtistName = data.getString(Constants.JSON_ARTIST_NAME);
            }

            if (!data.isNull(Constants.JSON_TITLE)) {
                this.mTitle = data.getString(Constants.JSON_TITLE);
            }

            if (!data.isNull(Constants.JSON_SEARCH_TITLE)) {
                this.mSearchTitle = data.getString(Constants.JSON_SEARCH_TITLE);
            }

            if (!data.isNull(Constants.JSON_LENGTH)) {
                this.mLength = data.getString(Constants.JSON_LENGTH);
            }

            if (!data.isNull(Constants.JSON_COVER_ART)) {
                this.mCoverArt = data.getString(Constants.JSON_COVER_ART);
            }

            if (!data.isNull(Constants.JSON_COVER_ART_SOURCE)) {
                this.mCovertArtSource = data.getString(Constants.JSON_COVER_ART_SOURCE);
            }

        } catch (JSONException e) {
            Log.e(TAG, "Error parsing Json");
        }
    }

    public String getCoverArt(){
        return mCoverArt;
    }

    public String getSmallCovertArt(){
        return getCovertArtBySize("100x100");
    }

    public String getBigCovertArt(){
        return getCovertArtBySize("200x200");

    }

    /**
     * Fetch the details of the song album
     */
    public void fetchAlbumDetails(){
        if(mAlbumId != null){
            String url = Constants.SERVER_API_URL + GeneralUtils.appendHMACtoUrl(Constants.ENDPOINT_SONG + "/" + mAlbumId + Constants.ALBUM_URL, new ArrayList<NameValuePair>());
            MTGxStringRequest request = new MTGxStringRequest(Request.Method.GET,
                    url,
                    successAlbumDetailsListener,
                    errorListener);

            MTGxApplication.getInstance().addToRequestQueue(request);
        }
    }

    Response.Listener successAlbumDetailsListener = new Response.Listener() {

        @Override
        public void onResponse(Object response) {
            Log.d(TAG,response.toString());
        }
    };

    /**
     * Fetch the details of the song artist
     */
    public void fetchArtistDetails(){
        if(mArtistId != null) {
            String url = Constants.SERVER_API_URL + GeneralUtils.appendHMACtoUrl(Constants.ENDPOINT_SONG + "/" + mArtistId + Constants.ARTIST_URL, new ArrayList<NameValuePair>());
            MTGxStringRequest request = new MTGxStringRequest(Request.Method.GET,
                    url,
                    successArtistDetailsListener,
                    errorListener);

            MTGxApplication.getInstance().addToRequestQueue(request);
        }
    }

    Response.Listener successArtistDetailsListener = new Response.Listener() {

        @Override
        public void onResponse(Object response) {
            Log.d(TAG,response.toString());
        }
    };

    Response.ErrorListener errorListener = new Response.ErrorListener() {

        @Override
        public void onErrorResponse(VolleyError volleyError) {
        }
    };

    public boolean hasCovertArt(){
        return !TextUtils.isEmpty(mCoverArt);
    }

    public String getCovertArtBySize(String size){
        return Constants.SERVER_API_URL + Constants.ENDPOINT_IMAGES + "/"+size+"/" + getCoverArt();
    }

    public String getTitle(){
        return mTitle;
    }

    public String getArtistName(){
        return mArtistName;
    }

    public String getId(){
        return mId;
    }

    /**
     * Methods needed to implement Parcelable
     */

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(mId);
        out.writeString(mArtistId);
        out.writeString(mAlbumId);
        out.writeString(mArtistName);
        out.writeString(mTitle);
        out.writeString(mSearchTitle);
        out.writeString(mLength);
        out.writeString(mCoverArt);
        out.writeString(mCovertArtSource);
    }

    public static final Parcelable.Creator<Song> CREATOR
            = new Parcelable.Creator<Song>() {
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        public Song[] newArray(int size) {
            return new Song[size];
        }
    };

    private Song(Parcel in) {
        mId = in.readString();
        mArtistId = in.readString();
        mAlbumId = in.readString();
        mArtistName = in.readString();
        mTitle = in.readString();
        mSearchTitle = in.readString();
        mLength = in.readString();
        mCoverArt = in.readString();
        mCovertArtSource = in.readString();
    }
}
