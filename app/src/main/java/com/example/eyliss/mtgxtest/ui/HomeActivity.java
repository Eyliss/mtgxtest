package com.example.eyliss.mtgxtest.ui;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;
import com.example.eyliss.mtgxtest.R;
import com.example.eyliss.mtgxtest.io.Song;
import com.example.eyliss.mtgxtest.util.Constants;

public class HomeActivity extends RootActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks{

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer fragment
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer,(DrawerLayout) findViewById(R.id.drawer_layout));
    }

    /** Replace the container fragment depends of the item selected. An instance of the corresponding fragment is created.
     *
     * @param position of the navigation drawer item selected
     *
     */
    @Override
    public void onNavigationDrawerItemSelected(int position) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        switch (position){
            case Constants.MenuSection.SONGS:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, HomeFragment.newInstance(position+1))
                        .commit();
                break;
            case Constants.MenuSection.HELP:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, HelpFragment.newInstance(position+1))
                        .commit();
                break;
            case Constants.MenuSection.ABOUT:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, AboutFragment.newInstance(position+1))
                        .commit();
                break;
        }
    }

    /** Change the action bar title depends of the section attached
     *
     * @param number of the section attached
     *
     */
    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    /**
     * Restore action bar default values
     */
    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.home, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /** Start DetailsActivity with the selected song to show its details
     *
     * @param song the song we want to show the detail
     *
     */
    public void showSongDetails(Song song){

        Intent intent = new Intent(this,DetailsActivity.class);
        intent.putExtra(Constants.INTENT_SONG,song);
        startActivity(intent);
    }
}
