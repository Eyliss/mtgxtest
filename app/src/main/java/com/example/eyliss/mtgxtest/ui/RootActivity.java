package com.example.eyliss.mtgxtest.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.example.eyliss.mtgxtest.R;


/**
 * A Root Activity that contains the basic common elements and functions.
 */
public class RootActivity extends ActionBarActivity {

    public ProgressDialog mLoginProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.root_activity);
    }

    /**
     * Show a loading progress dialog to notice to the user some data is being loading.
     * The dialog is cancelable to avoid screen lock if a server error occurs when data is fetched
     *
     * @param dialogText to show in the progress dialog
     */
    public void showDialogFragment(String dialogText){
        if(mLoginProgressDialog == null)
            mLoginProgressDialog = new ProgressDialog(this);

        mLoginProgressDialog.setMessage(dialogText);
        mLoginProgressDialog.setCancelable(false);
        mLoginProgressDialog.show();
    }

    /**
     * Check if a progress dialog has been shown and hide it
     */
    public void hideDialogFragment(){
        if(mLoginProgressDialog != null)
            mLoginProgressDialog.dismiss();
    }
}