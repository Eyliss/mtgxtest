package com.example.eyliss.mtgxtest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.eyliss.mtgxtest.MTGxApplication;
import com.example.eyliss.mtgxtest.R;
import com.example.eyliss.mtgxtest.io.Song;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

public class SongAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private ArrayList<Song> mSongs = new ArrayList<Song>();

    public SongAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public SongAdapter(Context context, ArrayList<Song> songs) {
        mInflater = LayoutInflater.from(context);
        mSongs = songs;
    }

    public int getCount() {
        return mSongs.size();
    }

    public ArrayList<Song> getItems(){
        return mSongs;
    }

    public Song getItem(int position) {
        return mSongs.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.song_row_item, null);
            holder = new ViewHolder();
            holder.mCoverArt = (ImageView) convertView.findViewById(R.id.iv_song_cover_art);
            holder.mTitle = (TextView) convertView.findViewById(R.id.tv_song_title);
            holder.mArtistName = (TextView) convertView.findViewById(R.id.tv_song_artist_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Song song = mSongs.get(position);

        holder.mCoverArt.setImageDrawable(null);
        if (song.hasCovertArt())
            ImageLoader.getInstance().displayImage(song.getSmallCovertArt(), holder.mCoverArt,MTGxApplication.getInstance().getDisplayOptions());

        holder.mTitle.setText(song.getTitle());
        holder.mArtistName.setText(song.getArtistName());

        return convertView;
    }

    static class ViewHolder {
        ImageView mCoverArt;
        TextView mTitle;
        TextView mArtistName;
    }
}
