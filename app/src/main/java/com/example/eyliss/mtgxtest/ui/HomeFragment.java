package com.example.eyliss.mtgxtest.ui;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.eyliss.mtgxtest.MTGxApplication;
import com.example.eyliss.mtgxtest.R;
import com.example.eyliss.mtgxtest.adapters.SongAdapter;
import com.example.eyliss.mtgxtest.io.Song;
import com.example.eyliss.mtgxtest.network.MTGxStringRequest;
import com.example.eyliss.mtgxtest.util.Constants;
import com.example.eyliss.mtgxtest.util.GeneralUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * A fragment containing a simple view.
 */
public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    /**
     * The elements threshold for the infinite scroll, and the current page where we are when scrolling
     */
    private int visibleThreshold = 10;
    private int currentPage = 0;

    private HomeActivity mParentActivity;
    private SwipeRefreshLayout mSwipeLayout;

    private ListView mSongsList;
    private SongAdapter adapter;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static HomeFragment newInstance(int section) {
        HomeFragment fragment = new HomeFragment();

        //The fragment argument representing the section number for this fragment.
        Bundle arguments = new Bundle();
        arguments.putInt(Constants.INTENT_SECTION_NUMBER,section);
        fragment.setArguments(arguments);

        return fragment;
    }

    public HomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mParentActivity = (HomeActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        bindView(rootView);

        mParentActivity.showDialogFragment(getString(R.string.loading_data));
        fetchSongs();

        return rootView;
    }

    /**
     * Bind the home layout view to use them globally in the fragment
     *
     * @param rootView view inflated into the fragment container
     */
    private void bindView(View rootView){

        mSongsList = (ListView) rootView.findViewById(R.id.lv_songs);
        mSwipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);

        //Set the listener for our swipe layout and the color scheme to show when when it is swiped
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    /**
     * Fetch all currently playing song with a limit threshold and offset
     */
    private void fetchSongs(){

        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

        params.add(new BasicNameValuePair(Constants.PARAM_LIMIT,String.valueOf(visibleThreshold)));
        params.add(new BasicNameValuePair(Constants.PARAM_OFFSET,String.valueOf(currentPage * visibleThreshold)));

        String url = Constants.SERVER_API_URL + GeneralUtils.appendHMACtoUrl(Constants.ENDPOINT_SONG + Constants.CURRENT_URL, params);
        MTGxStringRequest request = new MTGxStringRequest(Request.Method.GET,
                url,
                successSongsListener,
                errorListener);

        MTGxApplication.getInstance().addToRequestQueue(request);
    }

    /**
     * Handle the current songs success requests from server.
     *
     * @return a {@link com.android.volley.Response.Listener Listener} reference.
     */
    Response.Listener successSongsListener = new Response.Listener() {

        @Override
        public void onResponse(Object response) {
            mParentActivity.hideDialogFragment();
            try {
                parseSuccessSongsResponse(new JSONArray(response.toString()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Handle the current songs error requests from server.
     *
     * @return a {@link com.android.volley.Response.ErrorListener ErrorListener} reference.
     */
    Response.ErrorListener errorListener = new Response.ErrorListener() {

        @Override
        public void onErrorResponse(VolleyError volleyError) {
            mParentActivity.hideDialogFragment();
            mSwipeLayout.setRefreshing(false);
            Log.e("HomeFragment", "An error has ocurred: " + volleyError.toString());
        }
    };

    /**
     * Parse the current songs response and creates a Song objects ArrayList.
     *
     * @param response received from the server with all the playing songs in JSON format
     */
    private void parseSuccessSongsResponse(JSONArray response){
        ArrayList<Song> songs = new ArrayList<Song>();
        try {
            for(int i = 0; i < response.length(); i++){
                songs.add(new Song(response.getJSONObject(i).getJSONObject(Constants.JSON_SONG)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        refreshSongsListAdapter(songs);
    }

    /**
     * Refresh songs adapter to implement infinite scroll listview:
     *
     * It creates a new custom adapter for our songs list, set to the view list and set on scroll and on item click listeners.
     * If the adapter is already created, add the new items to the adapter list and notify this data change to the adapter.
     */
    private void refreshSongsListAdapter(ArrayList<Song> songs){
        mSwipeLayout.setRefreshing(false);

        if(adapter == null){

            adapter = new SongAdapter(mParentActivity, songs);
            mSongsList.setAdapter(adapter);
            mSongsList.setOnScrollListener(new EndlessScrollListener());
            mSongsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                    mParentActivity.showSongDetails(adapter.getItem(pos));
                }
            });

        }else{
            ArrayList<Song> items = adapter.getItems();
            items.addAll(songs);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((HomeActivity) activity).onSectionAttached(
                getArguments().getInt(Constants.INTENT_SECTION_NUMBER));
    }

    /**
     * On refresh listener for swipe layout. It sets the refreshing flag to true to notify the refresh to user in UI, reset adapter values
     * and call the download song task
     */
    @Override
    public void onRefresh() {
        mSwipeLayout.setRefreshing(true);

        currentPage = 0;
        adapter = null;

        new downloadSongs().execute("");
    }

    /**
     * Scroll listener for infinite scroll implementation. It compares the list last visible position and it download more elements if
     * this position is more than the number of visible elements.
     */
    public class EndlessScrollListener implements AbsListView.OnScrollListener {

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        }

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == SCROLL_STATE_IDLE) {
                if (mSongsList.getLastVisiblePosition() >= mSongsList.getCount() - visibleThreshold) {
                    currentPage++;
                    new downloadSongs().execute("");
                }
            }
        }

    }

    /**
     * AsyncTask for download songs from the server without interrupt the main thread of execution
     */
    private class downloadSongs extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            fetchSongs();
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }
}
