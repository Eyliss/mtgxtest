package com.example.eyliss.mtgxtest.ui;



import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.eyliss.mtgxtest.MTGxApplication;
import com.example.eyliss.mtgxtest.R;
import com.example.eyliss.mtgxtest.util.Constants;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class AboutFragment extends Fragment {

    private TextView mTvAppName;
    private TextView mTvAppVersion;

    public AboutFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static AboutFragment newInstance(int section) {
        AboutFragment fragment = new AboutFragment();

        //The fragment argument representing the section number for this fragment.
        Bundle arguments = new Bundle();
        arguments.putInt(Constants.INTENT_SECTION_NUMBER,section);
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.about_fragment, container, false);
        bindView(rootView);
        setVersion();
        return inflater.inflate(R.layout.about_fragment, container, false);
    }

    /**
     * Bind the home layout view to use them globally in the fragment
     *
     * @param rootView view inflated into the fragment container
     */
    private void bindView(View rootView){
        mTvAppName = (TextView) rootView.findViewById(R.id.tv_about_app_name);
        mTvAppVersion = (TextView) rootView.findViewById(R.id.tv_about_app_version);
    }

    /**
     * Set the app version to text view. The version name is got from the package info.
     */
    private void setVersion(){
        String appVersion = "Not available";
        try {
            appVersion = getActivity().getPackageManager()
                    .getPackageInfo(getActivity().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        mTvAppVersion.setText(getString(R.string.about_version,appVersion));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((HomeActivity) activity).onSectionAttached(
                getArguments().getInt(Constants.INTENT_SECTION_NUMBER));
    }
}
