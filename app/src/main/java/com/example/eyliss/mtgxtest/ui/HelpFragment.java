package com.example.eyliss.mtgxtest.ui;



import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eyliss.mtgxtest.R;
import com.example.eyliss.mtgxtest.util.Constants;

public class HelpFragment extends Fragment {

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static HelpFragment newInstance(int section) {
        HelpFragment fragment = new HelpFragment();

        //The fragment argument representing the section number for this fragment.
        Bundle arguments = new Bundle();
        arguments.putInt(Constants.INTENT_SECTION_NUMBER,section);
        fragment.setArguments(arguments);

        return fragment;
    }

    public HelpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.help_fragment, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((HomeActivity) activity).onSectionAttached(
                getArguments().getInt(Constants.INTENT_SECTION_NUMBER));
    }
}
