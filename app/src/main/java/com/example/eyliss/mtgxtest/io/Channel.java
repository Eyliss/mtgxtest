package com.example.eyliss.mtgxtest.io;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;

import com.example.eyliss.mtgxtest.util.Constants;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * The channel class. Represents the object and implements the necessary methods to create a channel and access its data.
 *
 */
public class Channel {

    public final String TAG = "Song";

    private String mId;
    private String mName;
    private String mDescription;
    private String mLogo;

    public Channel (){

    }

    public Channel(JSONObject album){
        super();
        updateFromJson(album);
    }


    public void updateFromJson(JSONObject data) {
        try {

            if (!data.isNull(Constants.JSON_ID)) {
                this.mId = data.getString(Constants.JSON_ID);
            }

            if (!data.isNull(Constants.JSON_NAME)) {
                this.mName = data.getString(Constants.JSON_NAME);
            }

            if (!data.isNull(Constants.JSON_DESCRIPTION)) {
                this.mDescription = data.getString(Constants.JSON_DESCRIPTION);
            }

            if (!data.isNull(Constants.JSON_LOGO)) {
                this.mLogo = data.getString(Constants.JSON_LOGO);
            }

        } catch (JSONException e) {
            Log.e(TAG, "Error parsing Json");
        }
    }

    public String getLogo(){
        return mLogo;
    }

    public String getSmallLogo(){
        return getCovertArtBySize("100x100");
    }

    public boolean hasLogo(){
        return !TextUtils.isEmpty(mLogo);
    }

    public String getCovertArtBySize(String size){
        return Constants.SERVER_API_URL + Constants.ENDPOINT_IMAGES + "/"+size+"/" + getLogo();
    }

    public String getName(){
        return mName;
    }

    public String getDescription(){
        return mDescription;
    }

    public String getId(){
        return mId;
    }

}