package com.example.eyliss.mtgxtest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.eyliss.mtgxtest.MTGxApplication;
import com.example.eyliss.mtgxtest.R;
import com.example.eyliss.mtgxtest.io.Channel;
import com.example.eyliss.mtgxtest.util.GeneralUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;


public class PlayedSongAdapter extends BaseAdapter{
    private LayoutInflater mInflater;
    private Context mContext;

    private ArrayList<String> mDates = new ArrayList<String>();
    private ArrayList<Channel> mChannels = new ArrayList<Channel>();

    public PlayedSongAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public PlayedSongAdapter(Context context, ArrayList<String> dates, ArrayList<Channel> channels) {
        mInflater = LayoutInflater.from(context);
        mContext = context;

        mDates = dates;
        mChannels = channels;
    }

    public int getCount() {
        return mDates.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.channel_played_row_item, null);
            holder = new ViewHolder();
            holder.mLogo = (ImageView) convertView.findViewById(R.id.iv_channel_logo);
            holder.mName = (TextView) convertView.findViewById(R.id.tv_channel_name);
            holder.mPublishedAt = (TextView) convertView.findViewById(R.id.tv_song_published_date);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String publishedAt = mDates.get(position);
        Channel song = mChannels.get(position);

        if (song.hasLogo())
            ImageLoader.getInstance().displayImage(song.getSmallLogo(),holder.mLogo, MTGxApplication.getInstance().getDisplayOptions());

        holder.mName.setText(song.getName());
        holder.mPublishedAt.setText(GeneralUtils.formatDate(publishedAt));

        return convertView;
    }

    static class ViewHolder {
        ImageView mLogo;
        TextView mName;
        TextView mPublishedAt;
    }
}
