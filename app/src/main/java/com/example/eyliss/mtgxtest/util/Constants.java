package com.example.eyliss.mtgxtest.util;

/**
 * Global constants needed throughout the application
 */
public class Constants {

    public class MenuSection {
        public static final int SONGS = 0;
        public static final int HELP = 1;
        public static final int ABOUT = 2;
    }

    public static final String INTENT_SECTION_NUMBER = "section_number";
    public static final String INTENT_SONG = "song";

    public static final String API_KEY = "bylSX9FNeyeG5hYP";
    public static final String API_SECRET = "ojfGdohIqLZz3PzjwJqsAgUw6owTPWvpGtpqUNq6sk627XKawJ";

    public static final String SERVER_API_URL = "http://stage.unison.mtgradio.se";
    public static final String ENDPOINT_SONG = "/api/v2/song";
    public static final String ENDPOINT_IMAGES = "/images";

    public static final String PLAYED_URL = "/played";
    public static final String ARTIST_URL = "/artist";
    public static final String ALBUM_URL = "/album";
    public static final String CURRENT_URL = "/current";

    public static final String PARAM_LIMIT = "limit";
    public static final String PARAM_OFFSET = "offset";
    public static final String PARAM_ORDER = "order";
    public static final String PARAM_SORT_ORDER = "sort_order";
    public static final String PARAM_WITH = "with";

    public static final String JSON_ID = "id";
    public static final String JSON_ARTIST_ID = "artist_id";
    public static final String JSON_ALBUM_ID = "album_id";
    public static final String JSON_ARTIST_NAME = "artist_name";
    public static final String JSON_TITLE = "title";
    public static final String JSON_SEARCH_TITLE = "search_title";
    public static final String JSON_LENGTH = "length";
    public static final String JSON_COVER_ART = "cover_art";
    public static final String JSON_COVER_ART_SOURCE = "cover_art_source";
    public static final String JSON_NAME = "name";
    public static final String JSON_DESCRIPTION = "description";
    public static final String JSON_LOGO = "logo";
    public static final String JSON_CHANNEL = "channel";
    public static final String JSON_SONG = "song";
    public static final String JSON_PUBLISHED_AT = "published_at";
}
